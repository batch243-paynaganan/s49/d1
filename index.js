// console.log('go sir hernan')

// * fetch()
// * this is a method in js that is used to send req in the server and load the information (res from the server) in the webpage
// todo Syntax
    //todo   fetch("urlAPI", {options/request from the user and response from the user})

//todo fetching all the elements in the db
let fetchPosts=()=>{
    fetch("https://jsonplaceholder.typicode.com/posts").then(response=>
        response.json().then(data=>{console.log(data)
        return showPosts(data)}))
}
fetchPosts()

const showPosts=(posts)=>{
    let postEntries='';
    posts.forEach((post)=>{
        postEntries+=`
        <div id="post-${post.id}">
            <h3 id="post-title-${post.id}">${post.title}</h3>
            <p id="post-body-${post.id}">${post.title}</p>
            <button onclick="editPost(${post.id})">Edit</button>
            <button onclick="deletePost(${post.id})">Delete</button>
        </div>
        `
    });
    document.querySelector('#div-post-entries').innerHTML =postEntries
}

//todo add post
document.querySelector('#form-add-post').addEventListener('submit',(event)=>{
    event.preventDefault()
    let title=document.querySelector('#txt-title').value
    let body=document.querySelector('#txt-body').value

    fetch('https://jsonplaceholder.typicode.com/posts',{
        method: "POST",
        body: JSON.stringify({
            title:title,
            body:body,
            userId: 1
        }),
        headers:{
            "Content-Type":"application/json"
        }
    })
    .then(response=>response.json())
    .then(data=>{console.log(data)})
    alert('Successfully added')

    document.querySelector('#txt-title').value=null;
    document.querySelector('#txt-body').value="";
})

// todo edit post
const editPost=(id)=>{
    let title = document.querySelector(`#post-title-${id}`).innerHTML
    let body = document.querySelector(`#post-body-${id}`).innerHTML
    
    document.querySelector('#txt-edit-title').value = title;
    document.querySelector('#txt-edit-body').value = body;
    document.querySelector('#txt-edit-id').value = id;
    //.removeAttriibute will remove the attrib 
    document.querySelector('#btn-submit-update').removeAttribute('disabled')
}

document.querySelector("#form-edit-post").addEventListener("submit",(event)=>{
    event.preventDefault()
    
    let id=document.querySelector('#txt-edit-id').value
    let title=document.querySelector('#txt-edit-title').value
    let body=document.querySelector('#txt-edit-body').value

    fetch(`https://jsonplaceholder.typicode.com/posts/${id}`,{
    method: "PATCH",
    body: JSON.stringify({
        title: title,
        body: body
    }),
    headers:{
        "Content-Type":"application/json"
    }
    })
    .then(response=>response.json())
    .then(data=>{
        console.log(data)
        alert('updated')})
    document.querySelector('#txt-edit-id').value=null;
    document.querySelector('#txt-edit-title').value=null;
    document.querySelector('#txt-edit-body').value=null;
    document.querySelector('#btn-submit-update').setAttribute('disabled',true)
})

const deletePost=async id=>{
    await fetch(`https://jsonplaceholder.typicode.com/posts/${id}`,{
        method: "DELETE"
    }).then(response=>response.json()).then(data=>{alert('post successfully deleted')})

    document.querySelector(`#post-${id}`).remove();
}
/* let specificPost=(id)=>{
    if(id<=100 && id>=1){
        fetch(`https://jsonplaceholder.typicode.com/posts/${id}`)
        .then(response=>response.json())
        .then(data=>console.log(data))
    }
}
specificPost(5) */